package com.yomi.abcnews.api

import androidx.lifecycle.MutableLiveData
import com.yomi.abcnews.model.FeedResponse
import com.yomi.abcnews.network.Resource

interface APIContract {
    fun getFeed(): MutableLiveData<Resource<FeedResponse>>
}