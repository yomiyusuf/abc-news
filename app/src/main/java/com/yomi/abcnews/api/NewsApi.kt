package com.yomi.abcnews.api

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyLog
import com.android.volley.toolbox.JsonObjectRequest
import com.yomi.abcnews.app.AppController
import com.yomi.abcnews.model.Enclosure
import com.yomi.abcnews.model.Feed
import com.yomi.abcnews.model.FeedResponse
import com.yomi.abcnews.model.NewsItem
import com.yomi.abcnews.network.AppException
import com.yomi.abcnews.network.Resource
import com.yomi.abcnews.network.Resource.Companion.SERVER_ERROR
import org.json.JSONObject

class NewsApi : APIContract{

    private var feedResource = MutableLiveData<Resource<FeedResponse>>()

    private val tag_json_obj = "json_obj_req"
    private val TAG = "NewsApi"

    override fun getFeed(): MutableLiveData<Resource<FeedResponse>> {
        makeRequest()
        return feedResource
    }

    private fun makeRequest(){
        val url = "https://api.rss2json.com/v1/api.json?rss_url=http://www.abc.net.au/news/feed/51120/rss.xml"
        val jsonObjReq = JsonObjectRequest(
            Request.Method.GET,
            url,
            null,
            Response.Listener { response ->
                if (response != null){
                    feedResource.value = Resource.success(convertToFeedResponse(response))
                    Log.e(TAG, convertToFeedResponse(response).toString())
                } else {
                    feedResource.value = Resource.error(null, SERVER_ERROR)
                }
            }, Response.ErrorListener { error ->
                Log.e(TAG, "Error: " + error.message)
                val exception = AppException(error)
                feedResource.value = Resource.error(exception = exception, msg = error.message.toString())
            })
        jsonObjReq.setShouldCache(false)

        AppController.instance?.addToRequestQueue(jsonObjReq, tag_json_obj)
    }

    private fun convertToFeedResponse(response: JSONObject) : FeedResponse{
        val status = response.optString("status")
        val feed = Feed(response.getJSONObject("feed")?.optString("title"))
        val itemsArray = response.getJSONArray("items")
        val newsItems = mutableListOf<NewsItem>()
        for (i in 0 until itemsArray.length()){
            val item = itemsArray[i] as JSONObject
            val newsItem = NewsItem(
                item.getString("content"),
                Enclosure(item.getJSONObject("enclosure").optString("link")),
                item.optString("link"),
                item.optString("pubDate"),
                item.optString("thumbnail"),
                item.optString("title")
            )
            newsItems.add(newsItem)
        }
        return FeedResponse(status, feed, newsItems)
    }
}