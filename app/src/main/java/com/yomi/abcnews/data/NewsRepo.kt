package com.yomi.abcnews.data

import androidx.lifecycle.MutableLiveData
import com.yomi.abcnews.api.APIContract
import com.yomi.abcnews.api.NewsApi
import com.yomi.abcnews.model.FeedResponse
import com.yomi.abcnews.network.Resource

class NewsRepo (){
    private var service: APIContract

    init {
        service = NewsApi()
    }

    fun getNewsFeed(): MutableLiveData<Resource<FeedResponse>> {
        return service.getFeed()
    }
}