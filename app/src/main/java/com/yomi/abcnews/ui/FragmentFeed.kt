package com.yomi.abcnews.ui


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

import com.yomi.abcnews.R
import com.yomi.abcnews.ui.adapter.NewsFeedAdapter
import com.yomi.abcnews.model.FeedResponse
import com.yomi.abcnews.network.Resource
import com.yomi.abcnews.viewModel.NewsFeedViewModel
import kotlinx.android.synthetic.main.fragment_feed.*

class FragmentFeed : Fragment(), SwipeRefreshLayout.OnRefreshListener {

    private lateinit var viewmodel: NewsFeedViewModel
    private lateinit var feedAdapter: NewsFeedAdapter
    val TAG = "FragmentFeed"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewmodel = ViewModelProviders.of(activity!!).get(NewsFeedViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_feed, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        feedAdapter = NewsFeedAdapter(context!!)
        refreshLayout_feed.setOnRefreshListener(this)
        setupRecyclerView()
        getData()
    }

    override fun onResume() {
        super.onResume()
        getData()
    }

    private fun setupRecyclerView() {
        val lm = LinearLayoutManager(context)
        rv_feed_list.layoutManager = lm
        rv_feed_list.addItemDecoration(ItemDecorationMargin(
            resources.getDimension(R.dimen.default_padding).toInt()))
        rv_feed_list.adapter = feedAdapter
    }

    private fun getData() {
        feedAdapter.setData(emptyList())
        progressbar_feed.show()
        viewmodel.getFactsData().observe(this, Observer { resource ->
            refreshLayout_feed.isRefreshing = false
            progressbar_feed.hide()
            if (resource?.data != null) {
                if (resource.data.isValid()) {
                    when (resource.status) {
                        Resource.Status.SUCCESS -> {
                            Log.d(TAG, "Number of news items is ${resource.data.items?.size}")
                            showFacts(resource.data)
                        }

                        Resource.Status.ERROR -> {
                            processError(resource)
                        }
                    }
                }
            }else{
                processError(resource)
            }
        })
    }

    private fun processError(resource: Resource<FeedResponse>) {
        Log.e(TAG, "Error is: ${resource.exception?.message}")
        when (resource.errorType) {
            Resource.ErrorType.NETWORK_UNAVAILABLE -> {
                Log.e(TAG, "No internet")
                showError(getString(R.string.no_internet))
            }

            Resource.ErrorType.SERVER_ERROR -> {
                Log.e(TAG, "Server problem")
                showError(getString(R.string.server_error))
            }
        }
    }

    private fun showFacts(data: FeedResponse) {
        (activity as ActivityMain).setFeedTitle(data.feed?.title()?: "")
        feedAdapter.setData(data.items)
    }

    private fun showError(msg: String){
        Toast.makeText(context!!, msg, Toast.LENGTH_SHORT).show()
    }

    override fun onRefresh() {
        getData()
    }

}
