package com.yomi.abcnews.ui

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.yomi.abcnews.app.AppController
import com.yomi.abcnews.model.NewsItem
import kotlinx.android.synthetic.main.card_news_small.view.*
import android.provider.MediaStore.Images.Media.getBitmap
import com.android.volley.toolbox.ImageLoader.ImageContainer
import com.android.volley.VolleyError
import com.android.volley.toolbox.ImageLoader.ImageListener
import android.util.Log
import android.view.View
import com.yomi.abcnews.R
import kotlinx.android.synthetic.main.card_news_large.view.*
import kotlinx.android.synthetic.main.card_news_small.view.txt_news_time


class NewsCardLarge: ConstraintLayout {

    val TAG = "NewsCardLarge"

    constructor(context: Context): super(context)

    constructor(context: Context, attrSet: AttributeSet): super(context, attrSet)

    constructor(context: Context, attrSet: AttributeSet, defStyleAttr: Int): super(context, attrSet, defStyleAttr)

    init {
        LayoutInflater.from(context)
            .inflate(R.layout.card_news_large, this, true)
    }

    fun setView(newsItem: NewsItem){
        resetView()
        val imageLoader = AppController.instance?.imageLoader
        newsItem.apply {
            txt_news_large_title.text = title
            txt_news_large_date.text = date()
            txt_news_large_time.text = time()
        }

        imageLoader?.get(newsItem.imageUrl(), object : ImageListener {

            override fun onErrorResponse(error: VolleyError) {
                Log.e(TAG, "Image Load Error: " + error.message)
            }

            override fun onResponse(response: ImageContainer, arg1: Boolean) {
                if (response.bitmap != null) {
                    image_news_large_thumbnail.setImageBitmap(response.bitmap)
                }
            }
        })

        if (newsItem.imageUrl() == ""){
            image_news_large_thumbnail.visibility = View.GONE
        }
    }

    private fun resetView(){
        image_news_large_thumbnail.visibility = View.VISIBLE
    }

    companion object{
        val VIEW_TYPE = 1
    }
}