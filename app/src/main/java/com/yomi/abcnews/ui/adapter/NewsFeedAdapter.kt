package com.yomi.abcnews.ui.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yomi.abcnews.model.NewsItem
import com.yomi.abcnews.ui.NewsCardLarge
import com.yomi.abcnews.ui.NewsCardSmall

class NewsFeedAdapter(private val context: Context):
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var newsItems = listOf<NewsItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var itemView = View(context)
        when(viewType){
            NewsCardLarge.VIEW_TYPE -> {
                itemView = NewsCardLarge(context)

                itemView.layoutParams = ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT)
                return LargeViewHolder(itemView)
            }

            NewsCardSmall.VIEW_TYPE -> {
                itemView = NewsCardSmall(context)

                itemView.layoutParams = ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT)
                return SmallViewHolder(itemView)
            }
        }

        itemView.layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT)
        return SmallViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return newsItems.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0){
            NewsCardLarge.VIEW_TYPE
        } else {
            NewsCardSmall.VIEW_TYPE
        }
    }

    fun setData(factList: List<NewsItem>?) {
        factList?.run { newsItems = this }
        this.notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(getItemViewType(position)){
            NewsCardLarge.VIEW_TYPE -> (holder as LargeViewHolder).newsItemLarge.setView(newsItems[position])
            NewsCardSmall.VIEW_TYPE -> (holder as SmallViewHolder).newsItemSmall.setView(newsItems[position])
        }
    }

    inner class LargeViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val newsItemLarge: NewsCardLarge

        init {
            newsItemLarge = view as NewsCardLarge
        }
    }

    inner class SmallViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val newsItemSmall: NewsCardSmall

        init {
            newsItemSmall = view as NewsCardSmall
        }
    }
}