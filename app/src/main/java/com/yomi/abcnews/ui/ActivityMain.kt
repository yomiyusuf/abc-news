package com.yomi.abcnews.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.yomi.abcnews.R


class ActivityMain : AppCompatActivity() {
    val fm = supportFragmentManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        loadListFragment()
    }

    fun setFeedTitle(title: String){
        supportActionBar?.title = title
    }

    private fun loadListFragment(){
        startFragment(FragmentFeed(), "FeedList")
    }

    private fun startFragment(fragment: Fragment, tag: String){
        fm.beginTransaction()
            .replace(R.id.main_content, fragment, tag)
            .commit()
    }
}
