package com.yomi.abcnews.model

import java.text.SimpleDateFormat

data class FeedResponse(
    val status: String?,
    val feed: Feed?,
    val items: List<NewsItem>?
){
    fun isValid() = status == "ok"

}

data class NewsItem(
    val content: String?,
    val enclosure: Enclosure?,
    val link: String?,
    val pubDate: String? = "",
    val thumbnail: String?,
    val title: String? = ""
){
    fun thumbnailUrl() = thumbnail?: ""

    fun imageUrl() = enclosure?.link?: ""

    fun date(): String{
        val formatter = SimpleDateFormat("MMM dd, yyyy")
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val date = dateFormat.parse(pubDate)
        return formatter.format(date).replace(".", "")
    }

    fun time(): String {
        val formatter = SimpleDateFormat("HH:mm a")
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val date = dateFormat.parse(pubDate)
        return formatter.format(date).toUpperCase()
    }
}

data class Enclosure(
    val link: String?
)


data class Feed(
    val title: String?
){
    fun title() = title?: "News Feed"
}
