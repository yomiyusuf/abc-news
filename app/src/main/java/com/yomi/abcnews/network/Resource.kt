package com.yomi.abcnews.network

import android.util.Log

class Resource<T> private constructor(val status: Status, val data: T?, val exception: AppException?, val errorType: ErrorType? = null) {
    enum class Status {
        SUCCESS, ERROR, LOADING
    }

    enum class ErrorType {
        NETWORK_UNAVAILABLE, SERVER_ERROR
    }
    companion object {

        const val SERVER_ERROR = "ServerError"
        fun <T> success(data: T?): Resource<T> {
            return Resource(Status.SUCCESS, data, null)
        }
        fun <T> error(exception: AppException?, msg: String): Resource<T> {
            var type: ErrorType? = null
            if (msg == SERVER_ERROR) type = ErrorType.SERVER_ERROR
            type = if (msg.startsWith("java.net.UnknownHostException")) ErrorType.NETWORK_UNAVAILABLE else ErrorType.SERVER_ERROR
            Log.e("Resource", type.toString())
            return Resource(Status.ERROR,null, exception, type)
        }
        fun <T> loading(data: T?): Resource<T> {
            return Resource(Status.LOADING, data, null)
        }
    }
}

class AppException(val exception: Throwable?): Exception()