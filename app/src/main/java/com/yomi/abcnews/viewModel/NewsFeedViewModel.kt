package com.yomi.abcnews.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.yomi.abcnews.data.NewsRepo
import com.yomi.abcnews.model.FeedResponse
import com.yomi.abcnews.network.Resource

class NewsFeedViewModel : ViewModel(){

    private var newsRepo: NewsRepo
    private var feedResponse: LiveData<Resource<FeedResponse>>

    init {
        newsRepo = NewsRepo()
        feedResponse = MutableLiveData()
    }

    fun getFactsData(): LiveData<Resource<FeedResponse>> {
        feedResponse = newsRepo.getNewsFeed()
        return feedResponse
    }
}